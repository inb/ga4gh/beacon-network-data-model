/**
 * *****************************************************************************
 * Copyright (C) 2019 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.ga4gh.beacon.network.model.v110;

/**
 * @author Dmitry Repchevsky
 */

public class ServiceInfo {
    
    private String id;
    private String name;
    private String serviceType;
    private String apiVersion;
    private String serviceUrl;
    private Boolean entryPoint;
    private ServiceOrganization organization;
    private String description;
    private String version;
    private Boolean open;
    private String welcomeUrl;
    private String alternativeUrl;
    private String createDateTime;
    private String updateDateTime;

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(final String serviceType) {
        this.serviceType = serviceType;
    }

    public String getApiVersion() {
        return apiVersion;
    }

    public void setApiVersion(final String apiVersion) {
        this.apiVersion = apiVersion;
    }

    public String getServiceUrl() {
        return serviceUrl;
    }

    public void setServiceUrl(final String serviceUrl) {
        this.serviceUrl = serviceUrl;
    }
    
    public Boolean getEntryPoint() {
        return entryPoint;
    }

    public void setEntryPoint(final Boolean entryPoint) {
        this.entryPoint = entryPoint;
    }
    
    public ServiceOrganization getOrganization() {
        return organization;
    }

    public void setOrganization(final ServiceOrganization organization) {
        this.organization = organization;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(final String version) {
        this.version = version;
    }
    
    public Boolean getOpen() {
        return open;
    }

    public void setOpen(final Boolean open) {
        this.open = open;
    }

    public String getWelcomeUrl() {
        return welcomeUrl;
    }

    public void setWelcomeUrl(final String welcomeUrl) {
        this.welcomeUrl = welcomeUrl;
    }

    public String getAlternativeUrl() {
        return alternativeUrl;
    }

    public void setAlternativeUrl(final String alternativeUrl) {
        this.alternativeUrl = alternativeUrl;
    }

    public String getCreateDateTime() {
        return createDateTime;
    }

    public void setCreateDateTime(final String createDateTime) {
        this.createDateTime = createDateTime;
    }

    public String getUpdateDateTime() {
        return updateDateTime;
    }

    public void setUpdateDateTime(final String updateDateTime) {
        this.updateDateTime = updateDateTime;
    }

}
